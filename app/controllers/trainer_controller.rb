class TrainerController < ApplicationController

	def train_txt 

		content = ''
			File.open('training_set.txt', 'r') do |file|
 			content = file.read
		end

		training_set = content.split("\n")
		categories   = training_set.shift.split(',').map{|c| c.strip}

		classifier1 = ClassifierReborn::Bayes.new categories

		training_set.each do |a_line|
  		next if a_line.empty? || '#' == a_line.strip[0]
  		parts = a_line.strip.split(':')
  		classifier1.train(parts.first, parts.last)
		end

		classifier_snapshot = Marshal.dump classifier1
		#File.open("/home/lalit/Desktop/max/tweetset/config/classifier/snapshot.dat", "w") {|f| f.write(snapshot.force_encoding('UTF-8')) }
		File.open("classifier1.dat", "w") {|f| f.write(classifier_snapshot.force_encoding('UTF-8')) }


		my_input = params[:my_input]
		@inp = "\""+my_input+"\""
		@cat = classifier1.classify @inp
	end
	 

	def t_initial
		correct = params[:correct]
		cinput = params[:cinput]

		if correct.blank?

			data = File.read("classifier1.dat")
			trained_classifier = Marshal.load data
			my_input = params[:my_input]
			@inp = "\""+my_input+"\""
			@cat = trained_classifier.classify @inp
			File.open("training_set.txt", "a+") { |f| f.puts "#{@cat}"+":"+"#{@inp}"}

		else
			@ct = correct
			@inu = cinput

			data = File.read("classifier1.dat")
			trained_classifier = Marshal.load data

			trained_classifier.train("#{@ct}" , "#{@inu}")
			

			classifier_snapshot = Marshal.dump trained_classifier
			#File.open("snapshot.dat", "w") {|f| f.write(snapshot.force_encoding('UTF-8')) }
			File.open("classifier1.dat", "w") {|f| f.write(classifier_snapshot.force_encoding('UTF-8')) }
			@inc = @inu.chomp('"').reverse.chomp('"').reverse
			File.open("training_set.txt", "a+") { |f| f.puts "#{@ct}"+":"+" #{@inc}"}
		end
	end	


	def inp_train
	#	File.open("training_set.txt", "w") { |f| f.puts "#{@cat}"+":"+"#{@inp}/n"}

	end













end
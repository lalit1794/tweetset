class TwitterController < ApplicationController

	

	def get_client
		client = Twitter::REST::Client.new do |config|
    	config.consumer_key = "FOfdHXBin8ls17FeR8xarDDAm"
    	config.consumer_secret = "0c0dNc3mipPjgrF5NhLOMKbGqCIpoM43ucn2BK5nA8iyL3GdYY"
    	config.access_token = "753208015029800961-nHZUgXcExKgGXQqUU10xo3cRsoGMA2m"
    	config.access_token_secret = "iwiLwmci7EpVnk0VVR74GPVDPRQt5eituYi3aDBWj6wIC"
		end
	end	
	
	def show
		@search = get_client.search("to:@RailMinIndia -rt", until:'2016-07-21')
		@search.collect do |tweet|
			tweet = Tweet.find_or_create_by(tweet_id: tweet.id) do |u|	
				u.user_id = tweet.user.id
				u.user_name = tweet.user.name
				u.user_screen_name = tweet.user.screen_name 
				u.user_location = tweet.user.location 
				u.tweet_id = tweet.id 
				u.text = tweet.text 
				u.lang = tweet.lang 
				u.rt_count = tweet.retweet_count 
				u.url = tweet.uri 
				u.tweet_created_at = tweet.created_at 
			end
		end	
	end

	def train
		StuffClassifier::Bayes.open("urgent or not_urgent or appreciation or disparage or others") do |cls|
			cls.ignore_words = [ '@RailMinIndia' ]
			cls.train(:urgent, "@RailMinIndia @GM_NRly @drmncrald No water in Lko shatabdi express washroom, C14. Its been only 30 min since it started from Lucknow.")
			cls.train(:not_urgent, "@RailMinIndia @sureshpprabhu sir wanted to know progress of Jabalpur-Gondia gauge conversion project")
			cls.train(:not_urgent, "@RailMinIndia you also charge dynamic fare and superfast charge.this is not super fast it takes about 25 hour or more from darbhanga to delh")
			cls.train(:appreciation, "@RailMinIndia @DRMBilaspur @IR_IGCNI  THANKUU SO MUCHH SIR...FOR QUICK RESPINSE &amp;  HELP ME.....INDIAN RAILWAY GREAT")
			cls.train(:urgent, "@RailMinIndia  people are fighting verbally each other.")
			cls.train(:others, "@RailMinIndia @sureshpprabhu @gmwcrailway @CHANDANKURARIYA Pics of katni south station.(Wcr jabalpur). Arajakta https://t.co/3shCyJTKEy")
			cls.train(:disparage, "@RailMinIndia @sureshpprabhu sorry Suresh v r nt happy with IRCTC food. Der r no prffnl cooks in IRCTC. Just go and check the grnd reality")
			cls.train(:urgent, "@RailMinIndia @narendramodi  @sureshpprabhu sir please please please take some immediate steps on Railways in Banswara, Rajasthan @PMOIndia")
			cls.train(:others, "@RailMinIndia @ds2269 @IR_EDMECHG matter notified to concerned officials")
			cls.train(:urgent, "@RailMinIndia Am traveling in Trn No:11027 and PNR-8405263854.The food is charged double compared in your rate card.Please necessary action.")
			cls.train(:not_urgent, "@RailMinIndia Please make a concrete apron atleast for 1st platform at Kannur(CAN). Very pathetic situation. NO SWACHH BHARATH atthisstation")
			cls.train(:appreciation, "@RailMinIndia @sureshpprabhu  Thank you sir for providing general coach travellers Deen dayalu coach..You are doing marvellous job..")
			cls.train(:others, "@RailMinIndia - 4 months before reservation start, travel plan difficult &amp; high cancelation charge, new loot mthod applicable to mddle class")
			cls.train(:urgent, "@RailMinIndia. Stinking bathroom. 12554, bogy number s9. Very poor service by rail India.")
			cls.train(:others, "@railminindia @drmmgs 8410670051/9627598249")
			cls.train(:disparage, "@RailMinIndia 2 of 3: The journey was so horrible though we had full reservation seats because throughout the journey people boarded the...")
			cls.train(:appreciation, "@RailMinIndia @IR_EDMECHG @DrmChennai Thanks a lot. .. just now mechanic has attended and problem resolved... Thanks again...")
			cls.train(:urgent, "@RailMinIndia train 12012 coach c-6 a/c not functioning properly. Repeated request made")
			cls.save_state
			@categ = cls.classify("@RailMinIndia @drmbsbner @IR_ENHM Thanks")
		end	
		
	end
	
	def check
		my_input = params['my_input']
	end

	def train2
		classifier = ClassifierReborn::Bayes.new('urgent', 'not_urgent', 'appreciation', 'disparage', 'others')
		
		chk = classifier.train_urgent "@RailMinIndia @GM_NRly @drmncrald No water in Lko shatabdi express washroom, C14. Its been only 30 min since it started from Lucknow."
		classifier.train_not_urgent "@RailMinIndia @sureshpprabhu sir wanted to know progress of Jabalpur-Gondia gauge conversion project"
		classifier.train_not_urgent "@RailMinIndia you also charge dynamic fare and superfast charge.this is not super fast it takes about 25 hour or more from darbhanga to delhi"
		classifier.train_appreciation "@RailMinIndia @DRMBilaspur @IR_IGCNI  THANKUU SO MUCHH SIR...FOR QUICK RESPINSE &amp;  HELP ME.....INDIAN RAILWAY GREAT"
		classifier.train_urgent "@RailMinIndia  people are fighting verbally each other."
		classifier.train_others "@RailMinIndia @sureshpprabhu @gmwcrailway @CHANDANKURARIYA Pics of katni south station.(Wcr jabalpur). Arajakta https://t.co/3shCyJTKEy"
		classifier.train_disparage "@RailMinIndia @sureshpprabhu sorry Suresh v r nt happy with IRCTC food. Der r no prffnl cooks in IRCTC. Just go and check the grnd reality"
		classifier.train_urgent "@RailMinIndia @narendramodi  @sureshpprabhu sir please please please take some immediate steps on Railways in Banswara, Rajasthan @PMOIndia"
		chk
		classifier.train_others "@RailMinIndia @ds2269 @IR_EDMECHG matter notified to concerned officials"
		classifier.train_urgent "@RailMinIndia Am traveling in Trn No:11027 and PNR-8405263854.The food is charged double compared in your rate card.Please necessary action."
		classifier.train_appreciation "@RailMinIndia @sureshpprabhu  Thank you sir for providing general coach travellers Deen dayalu coach..You are doing marvellous job.."
		classifier.train_others "@RailMinIndia - 4 months before reservation start, travel plan difficult &amp; high cancelation charge, new loot mthod applicable to mddle class"
		classifier.train_urgent "@RailMinIndia. Stinking bathroom. 12554, bogy number s9. Very poor service by rail India."
		classifier.train_others "@railminindia @drmmgs 8410670051/9627598249"
		classifier.train_disparage "@RailMinIndia 2 of 3: The journey was so horrible though we had full reservation seats because throughout the journey people boarded the..."
		classifier.train_appreciation "@RailMinIndia @IR_EDMECHG @DrmChennai Thanks a lot. .. just now mechanic has attended and problem resolved... Thanks again..."
		classifier.train_urgent "@RailMinIndia train 12012 coach c-6 a/c not functioning properly. Repeated request made"
		classifier.train_disparage  "@RailMinIndia it's bad there is no charging point on seats in Sleeper Class in long rote train like Tamil Nadu Exp. 12621 PNR - 4533850366"
		classifier.train_not_urgent "@RailMinIndia @gmner_gkp @GM_ECRly sir Maine vinayak steel ka application diya  lekin ECR ke store department ne abhi tak refund nahi Kiya."
		classifier.train_not_urgent  "@RailMinIndia @IR_EDCHG @sureshpprabhu I hope my request n prayers shall reach to the concerned authority ...we do need Railways in banswara"
		classifier.train_not_urgent  "@RailMinIndia PNR 6152204753 Ticket cancelled 2 weeks ago from IRCTC app however don't have any refund status or refund credit.No Update."
		classifier.train_not_urgent  "@RailMinIndia @sureshpprabhu Sir pls develop Mau UP station for growth of Purvanch. requesting regarding infra and automation of indication"
		classifier.train_appreciation  "@RailMinIndia thanks sir for your quick response."
		classifier.train_disparage  "@RailMinIndia sir what is the use of complaining  when officials of JHS donot respond"
		classifier.train_disparage  "@RailMinIndia Rail ministet suresh prabhu still not implemented RB,rti policy of 11oct of GMs made recommedations of rail stoppages"
		classifier.train_not_urgent  "@RailMinIndia  there is no lights on the foot over bridge at nalasopara (vasai side) last four days."
		classifier.train_appreciation  "@RailMinIndia this should be in all railway station... Good effort by Punjab govt"
		classifier.train_urgent  "@RailMinIndia lifts not working@Ahmedabad station. St shall Sr citizens n patients do?"
		classifier.train_others  "@RailMinIndia @sureshpprabhu Aren't there ultrasonic detectors used in such cases?"
		classifier.train_appreciation  "@RailMinIndia we are proud for our railways. Keep going ahead.."
		classifier.train_urgent  "@RailMinIndia PNR-2420088310. No water in B-2 14035"
		classifier.train_not_urgent  "@RailMinIndia @sureshpprabhu plz review only one train gomti express also train took much time in outer due to platform not available."
		classifier.train_urgent  "@RailMinIndia  PNR 8249421438 my bag was stolen in train I went to GRP NU but he refused to lodge FIR he told go to GAR station to lodge FIR"
		classifier.train_not_urgent  "@RailMinIndia @sureshpprabhu  travelling through tn num 12141, Rail neer same bottle sold at 2 different prices(1/2)"
		classifier.train_others "@RailMinIndia https://t.co/gVLCYN5jta"
		classifier.train_others  "@RailMinIndia @aaryanagrawal7 Corrective action is being initiated through @CSMC_WRly Pl share your PNR no."
		classifier.train_not_urgent  "@railminindia Shops on road at outside Jodhour Railway Station JU No one care about it https //t.co/DtXxYFI7i6"
		classifier.train_not_urgent  "@RailMinIndia either you run 9 coach or 12 coach completely  on all Mumbai local train routes as it creates nonsense for passengers."
		classifier.train_disparage  "@RailMinIndia half of the time it is switched off"
		classifier.train_urgent  "@RailMinIndia @drmmgs @drmncrald @IR_EDMECHG This is not wat I expected..S5 was for d sake cleaned,still having inconvenience.smelly &amp; dirty"
		classifier.train_not_urgent  "@RailMinIndia (Irctc #3709785) Full refund not received on TDR of PNR 2417282363 when train(12716) was late More than 3 hours.(6hrs 40min)."
		classifier.train_urgent  "@RailMinIndia Dear Sir please provide us seat in Gorakhdham it's urgent."
		classifier.train_appreciation  "@RailMinIndia @VlnVijay Sir, Thanks for calling us."
		classifier.train_urgent  "@RailMinIndia Train running late more than 3 hours.Can I cancel the window ticket before the schedule departure? https://t.co/ImYFlFMFiZ"
		classifier.train_disparage  "@RailminIndiau frwrded my rqst twice to @drm_kir about my father's illness bt nothing hpnd why ?"
		classifier.train_others  "@RailMinIndia @sureshpprabhu I a read rail Engine missing at Tuglaqabad. a 20C rs.machine cant afford 20rs ?GPS.India need rapid change."
		classifier.train_appreciation  "@RailMinIndia @sureshpprabhu -very nice idea with willingful efforts, congrats!"
		classifier.train_urgent  "@RailMinIndia  Berths and coach are dirty. S1 of 12486 Nanded Exp. PNR 2648731528."
		classifier.train_not_urgent  "@RailMinIndia @nimitjainjpr Sir, FIR lodged by GRP/Sawai madhopur vide Cr.no. 57/16 U/S 380 IPC."
		classifier.train_urgent  "@RailMinIndia even if he is a senior citizen he did not get lower birth.pls help as he cannot get up in the upper &amp; middle birth."
		classifier.train_others  "@RailMinIndia when the results of the RRB 03/2015 be decalred eagerly waiting for it thanks."
		classifier.train_others  "@RailMinIndia can provide such facilities but its Public's responsibility to use them as if its their own property https://t.co/waDElVKRFD"
		classifier.train_urgent  "@RailMinIndia PNR: 2320515242, Train No. 14155, AC not working. Please resolve this."
		classifier.train_disparage  "@RailMinIndia @GMSECR @drm_raipur  very poor maintenance of railway over bridge near power house railway station bhilai no proper road."
		classifier.train_urgent  "@RailMinIndia I'm on uttarbanga express now, but I'm not allowed to seat on my own seat plz help"
		classifier.train_urgent  "@RailMinIndia @IR_IGCNI @propgt14 RPF/Calicut secured one mobile phone   at Calicut  Platform  and handed over. https://t.co/GlPnIolYi0"
		classifier.train_urgent  "@RailMinIndia Dear sir some political caddars occupied our seat. We are help less. Please do something.PNR:6757405743, A GOSWAMI,S4 72,."
		classifier.train_not_urgent  "@RailMinIndia @IRCTC_Ltd   Is this fair to block my 8 years old irctc account for using auto fill feature? Login id: hasanfak"
		classifier.train_urgent  "@RailMinIndia pnr number 615216632 . Attender denied to help. On 138 they asked to speak with attender."
		classifier.train_urgent  "@RailMinIndia @nishantkenator Sir, u r requested to lodge FIR for legal action. Thanks."
		classifier.train_not_urgent  "@RailMinIndia @sureshpprabhu evng trains DLI to MTJ route daily pasngr Trains Which time daily pasngr goes to home plz think abt daily psngr"
		classifier.train_others "@RailMinIndia @harshmita_singh Please send your contact number or journey (pnr) details for further action."
		classifier.train_appreciation  "@RailMinIndia @Masood123452 Sir, happy to hear that missing lady has reached her home safely. Thanks for calling us."
		classifier.train_others  "@RailMinIndia rolls out modern unreserved coach with latest amenities: Check out features  https://t.co/lCKI9qug2k"
		classifier.train_others "@RailMinIndia @sureshpprabhu people should keep the Rly property as it is as they will only be using the same. I request all Indians 4 same"
		classifier.train_appreciation  "@railminindia @drmjabalpur @rkjha956 nobody in this world is using #tweeter better than #India railway"
		classifier.train_urgent  "@RailMinIndia I am traveling in 17210,reached BZA, still the RAC Passengers didn't get the berth,when we can expect Berth allocation."
		classifier.train_urgent  "@RailMinIndia A lot of people without  ticket boarded S1 in Train#12687 and they are causing a lot of inconvenience to the people"
		classifier.train_urgent  "@RailMinIndia Passenger On 69 Is Private Engineer As Per Him Pump Is Not Functioning Properly.....Now Staff Started Same Thing"
		classifier.train_others  "@RailMinIndia @sureshpprabhu  use solar panels, in railway station&amp;platform rooftop from Mumbai to Chennai route, high sun visibility,"
		classifier.train_disparage  "@RailMinIndia @sureshpprabhu my wife had to go to toilet after crossing 3 bogies in search of hygiene. And believe me we regular by train.??"
		classifier.train_others  "@RailMinIndia @rpfsecr1  comp no-2095 plz make conversation with complainant if required take n/a &amp; intimate this office."
		classifier.train_urgent  "@RailMinIndia skybag with laptop, documents of Raghav was lost train 18246, B3-41. need urgent help- 9873377945, going for admission"
		classifier.train_appreciation  "@RailMinIndia @sureshpprabhu Sir We all Salute For Ur Honest &amp; Hardworking in the interest of our Nation"
		classifier.train_not_urgent  "@RailMinIndia Open smoking railway platforms including belgharia children woman badly affected  railway silent vigilance check RPF role"
		classifier.train_others  "@RailMinIndia sir will this trial be conducted on New delhi -mugalsarai route as well??"
		classifier.train_disparage  "@RailMinIndia @narendramodi please do something to improve the punctuation. Rly ministry should take lesson from other countries."
		classifier.train_urgent  "@RailMinIndia pnr no. 6757046881, cleanliness and water problem in the coaches of Northeast express"
		classifier.train_not_urgent  "@RailMinIndia  Nt a single intetcity Train frm Bhuj 2 remaining Gujrat, other side bst in class travel bus operators on route@vinodchavda31"
		classifier.train_disparage  "@RailMinIndia Which Train ? , We southindians normally dont get anysuch coaches, very old , damaged coaches only (especially kerala side)"
		classifier.train_others  "@RailMinIndia @sureshpprabhu Pls do do not provide free water as it will be wasted and misused. Pls charge min Rs.2/Ltr..Dustbin required"
		classifier.train_not_urgent  "@RailMinIndia in case of railway cancelled. There should be prior intimation. If provided message before 3 hours. How passenger can manage?"
		classifier.train_others  "@RailMinIndia @sureshpprabhu pls  install camera also so that people dont ruin railway property."
		classifier.train_appreciation  "@RailMinIndia @sureshpprabhu always innovative"
		classifier.train_others  "@RailMinIndia @Sandesh29684 Matter notified to concerned official"
		classifier.train_others  "@RailMinIndia @gagandeep122 @IR_EDCHG"
		classifier.train_appreciation  "@RailMinIndia It's nice to see your response to each and every person....Now I feel that we have the best government in power......"
		classifier.train_appreciation  "@RailMinIndia sorry sir i frgt to mention my pnr last night bt jst few minutes back a sweeper cleaned the toilets. Thnk u."
		classifier.train_urgent  "@RailMinIndia @drmmumbaicr @IR_EDMECHG water also is not coming in toilet."
		classifier.train_not_urgent  "It's better to maintain the old escalators rather than installing new. @kanpurcentral https://t.co/JsNvGkawaS"
		classifier.train_appreciation  "@RailMinIndia @sureshpprabhu @EconomicTimes tq for your support to ap looking forward..we know you will fulfill Vizag railway zone too"
		classifier.train_not_urgent  "@RailMinIndia @sureshpprabhu wat about abusive comments written in toilets even if first class sir any measures taken for the same ."
		classifier.train_others  "@RailMinIndia please joint more general coaches in long distance trains"
		classifier.train_others  "@RailMinIndia @sureshpprabhu why railways is not trying to earn money by leasing the name of stations to corporates - like SBI Howrah!"
		classifier.train_not_urgent  "@RailMinIndia  the train 54022 is daily reaching the bulandshar minimum 01 hour late. daily passenger are effective from this train timing"
		classifier.train_urgent  "@RailMinIndia  Sir, I want to talk my father I could not contact him pls help me.PNR 442-1655589R V Teraiya B4 52 M 63 My Mo. 09726041363"
		classifier.train_appreciation  "@RailMinIndia @DRM_ASN hey thanks, your man reached and solved the issue...they even repaired some more switch boarded..?? thanks"
		classifier.train_urgent  "@RailMinIndia  allow him with xerox ticket, he is extremly poor , sir plz"
		classifier.train_appreciation  "@RailMinIndia thanks for the help, fans are working now. Can't imagine finally things like this happening in India. Thank you sir, keep goin"
		classifier.train_disparage  "@RailMinIndia Situation of Dadar to Badlapur travel by train is worst. Heartbreaking to see working woman's struggle to reach home. Stop it."
		classifier.train_not_urgent  "@RailMinIndia On irctc site I cannot access ID Card Type in booking as Tab is not functioning. Please rectify"
		classifier.train_others  "@RailMinIndia @cscrpfner_gkp Comp No.2087 Pl make conversation with complainant if required, take n/a and intimate to this office."
		classifier.train_appreciation "@RailMinIndia @IR_IGCNI  tq for ur concern please look  into the issue ASAP my suitcase is very important for me"
		classifier.train_not_urgent  "@RailMinIndia 12695 trivandrum superfast exp held up in palakkad for 22207 super ac express to overtake... why delay one train for another?"
		classifier.train_not_urgent  "@RailMinIndia @drmmumbaicr Please look after the number of issues at the Seawood Station, Navi-Mumbai"
		classifier.train_urgent  "@RailMinIndia Staff Of 12302 NDLA To HWH Rajdhani Express Trying To Arrange Water In Toilet Of B-1 Staff In Corridor https://t.co/KAdOYzong6"
		classifier.train_others  "@RailMinIndia https://t.co/dESMYiGheG"
		classifier.train_not_urgent  "@RailMinIndia @sureshpprabhu Still we have seen filthy toilets in Happa xpress of 18th July feom Tirunelvelu coach s2 &amp; s1. What abt dat???"
		classifier.train_appreciation  "@RailMinIndia Nice work.. Hope we Indians will keep these facilities intact and use them properly."
		classifier.train_others  "@RailMinIndia @nitincmc Corrective action is being initiated through @nr_ctg Kindly share your journey details PNR etc."
		classifier.train_disparage  "@RailMinIndia labours at nahur station collecting garbage frm 1 track &amp; throwing besides another track behind plants. Whr is swacch Bharat?"
		classifier.train_others  "@RailMinIndia @WesternRly @wc_railway @Central_Railway @sureshpprabhu https://t.co/56lGxANrAF"
		classifier.train_others  "@RailMinIndia @sureshpprabhu  What about airport like security across major railway stations in India???"
		classifier.train_disparage  "@RailMinIndia @drmlko25 @GM_NRly Sir 139 CONFIRMED they dont have this (Coach Postn) status with them. Plz avoid giving wrong info. Thanks"
		classifier.train_not_urgent  "@RailMinIndia Train#14203 condition of all seats in the coach is tragic. Charging port aint workin @sureshpprabhu https://t.co/VVVTaTqwZH"
		classifier.train_urgent  "@RailMinIndia @GM_NRly @indianrai currently she is gen boggi near guard Cabin at back side she don't have mobile n have backache  prob."
		classifier.train_not_urgent  "@RailMinIndia Banswara, Rajasthan a district with over 7 lac+ population is still not connected by Railways..kindly forward this to prabhuji"
		classifier.train_appreciation "@RailMinIndia @drmsecunderabad @IR_EDMECHG I really appreciate for your quick response. Thanks a ton??"
		classifier.train_not_urgent  "@RailMinIndia @sureshpprabhu  please look into  quota for Mumbai Express, seats are waitlisted for kumta pax  in lean season, train empty"
		classifier.train_disparage  "@RailMinIndia @sureshpprabhu HAVE A LOOK OF KALYAN STATION IW IS TOTALLY FAILURE EVERY SINGLE ROOF IS HAVE LEAKEGE Y https://t.co/CjHvwVpgxz"


		classifier_snapshot = Marshal.dump classifier
		#File.open("/home/lalit/Desktop/max/tweetset/config/classifier/snapshot.dat", "w") {|f| f.write(snapshot.force_encoding('UTF-8')) }
		File.open("classifier.dat", "w") {|f| f.write(classifier_snapshot.force_encoding('UTF-8')) }


		my_input = params[:my_input]
		@inp = "\""+my_input+"\""
		@cat = classifier.classify @inp
	end	

	def initial
		correct = params[:correct]
		cinput = params[:cinput]

		if correct.blank?

			data = File.read("classifier.dat")
			trained_classifier = Marshal.load data
			my_input = params[:my_input]
			@inp = "\""+my_input+"\""
			@cat = trained_classifier.classify @inp

		else
			@ct = correct
			@inu = cinput

			data = File.read("classifier.dat")
			trained_classifier = Marshal.load data

			trained_classifier.train("#{@ct}" , "#{@inu}")
			

			#trained_classifier.train_urgent "@RailMinIndia there is no information of train no. 16787 pls give the information"
			classifier_snapshot = Marshal.dump trained_classifier
			#File.open("snapshot.dat", "w") {|f| f.write(snapshot.force_encoding('UTF-8')) }
			File.open("classifier.dat", "w") {|f| f.write(classifier_snapshot.force_encoding('UTF-8')) }

		end
	end	

	def inp_train
	end




end
class CreateTweets < ActiveRecord::Migration[5.0]
  def change
    create_table :tweets do |t|
      t.integer :user_id                :limit => 8
      t.string :user_location
      t.string :user_screen_name
      t.integer :tweet_id               :limit => 8
      t.string :lang
      t.integer :rt_count
      t.string :text
      t.text :url
      t.datetime :tweet_created_at

      t.timestamps
    end
  end
end

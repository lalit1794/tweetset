class AddcolumnsInTweets < ActiveRecord::Migration[5.0]
  def change
  	add_column :tweets, :in_reply_to_status_id, :integer, limit:8
  	add_column :tweets, :in_reply_to_screen_name, :string
  end
end

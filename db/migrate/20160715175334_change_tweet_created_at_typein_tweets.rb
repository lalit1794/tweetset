class ChangeTweetCreatedAtTypeinTweets < ActiveRecord::Migration[5.0]
  def change
  	change_column :tweets, :tweet_created_at, :datetime
  	change_column :tweets, :user_id, :integer, limit:8
  	change_column :tweets, :tweet_id, :integer, limit:8
  	add_column :tweets, :user_name, :string
  end

end

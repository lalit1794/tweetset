class ConvertDatabaseToUtf8mb4 < ActiveRecord::Migration[5.0]
  def change
  	execute "ALTER TABLE tweets CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_bin"
   # execute "ALTER TABLE tweets CHANGE text string(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin"
  end
end
